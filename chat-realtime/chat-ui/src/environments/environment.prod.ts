export const environment = {
  production: true,
  url: 'http://10.5.0.1:8090/chat/v1.0/',
  urlsocket: 'http://10.5.0.1:8090/ws-chat/',
};
