import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';

export class WebSocketAPI {
    webSocketEndPoint: string = environment.urlsocket;
    stompClient: any;
    appComponent: AppComponent;

    constructor(appComponent: AppComponent){
        this.appComponent = appComponent;
    }

    _connect(topic) {
        console.log("Initialize WebSocket Connection");
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.stompClient.connect({}, function (frame) {
            _this.stompClient.subscribe("/topic/group/" + topic, function (sdkEvent) {
                _this.onMessageReceived(sdkEvent);
            });
            _this.stompClient.reconnect_delay = 20;
        }, this.errorCallBack);
    };

    _disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log("Disconnected");
    }

    // on error, schedule a reconnection attempt
    errorCallBack(error, topic) {
        console.log("errorCallBack -> " + error)
        setTimeout(() => {
            this._connect("/topic/group/" + topic);
        }, 5000);
    }

	/**
	 * Send message to sever via web socket
	 * @param {*} message 
	 */
    // _send(message) {
    //     console.log("calling logout api via web socket");
    //     this.stompClient.send("/topic/group/12345678910", {}, JSON.stringify(message));
    // }

    onMessageReceived(message) {
        console.log("message", message);
        this.appComponent.handleMessage(message.body);
    }
}