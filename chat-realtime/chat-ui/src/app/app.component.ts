import { AfterViewChecked, ElementRef, Component, OnInit, ViewChild } from '@angular/core';
import { WebSocketAPI } from './WebSocketAPI';
import { ChatService } from './chat.service';
import { empty } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit, AfterViewChecked  {
  @ViewChild('scrollMe', null) private myScrollContainer: ElementRef;

  webSocketAPI: WebSocketAPI;
  clientName = '';
  newMessage = '';
  chatActive = [];
  chats = [];
  contents = [];
  serverName = 'Admin';
  topic: string;
 
  constructor(
    private service: ChatService,
  ) { }

	ngOnInit() {
    this.initializeWebSocketConnection();
    this.getMessage();
    this.scrollToBottom();
  }

  initializeWebSocketConnection() {
    this.webSocketAPI = new WebSocketAPI(this);
    this.webSocketAPI._connect('create-topic');
  }

  getMessage() {
    this.service.getMessage({
      role: "ADMIN",
      type: "DEFAULT",
      page: 0
		}).subscribe(res => {
    console.log('response api get message',res);
    this.chats = res;
    this.chatActive = res;
    this.contents = res[0].listContent;
    this.clientName = res[0].clientInfo.clientName;
    this.topic = res[0].key;
    this.webSocketAPI._connect('send-msg');
    console.log('list content:', this.contents);
		}, err => {
			console.log(err)
		})
  }

  sendMessage() {
    var message = this.newMessage;
    this.newMessage = '';
    this.service.sendMessage({
			key: this.topic,
      role: 'ADMIN',
      type: 'DEFAULT',
      message: message,
      userId: "3",
      userName: this.serverName
		}).subscribe(res => {
    console.log(res)
		}, err => {
      console.log(err)
		})
  }

  chatSelect(key) {
     this.chatActive = this.chats.filter(function(chat) {
      return chat.key == key;
    })

    this.clientName = this.chatActive[0].clientInfo.clientName;
    this.contents = this.chatActive[0].listContent;
    this.topic = this.chatActive[0].key;
  }

  handleMessage(message){
    console.log("Message Recieved", JSON.parse(message));
    var msgUp = this.chats.filter(function(chat) {
      return chat.key == JSON.parse(message).key;
    })
    console.log("msgUp", msgUp);
    if (msgUp.length == 0) {
      this.createNewChat(JSON.parse(message));
    } else {
      this.chats = this.chats.filter(function(chat) {
        return chat.key !== JSON.parse(message).key;
      })

      msgUp[0].listContent.push(JSON.parse(message));
      this.chats.unshift(msgUp[0]);
    }
  }

  createNewChat(message) {
    this.service.getMessage({
      key: message.key,
      page: 0
		}).subscribe(res => {
    console.log('response api get message',res);
    this.chats.unshift(res[0]);
    this.webSocketAPI._connect('send-msg');
		}, err => {
			console.log(err)
		})
  }

  ngAfterViewChecked() {        
    this.scrollToBottom();        
} 

  scrollToBottom(): void {
    try {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }                 
  }
}

