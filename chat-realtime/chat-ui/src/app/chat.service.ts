import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ChatService {
	url = environment.url

	constructor(
		private http: HttpClient,
	) {
    }

	getHttpOption(){
		return {
			headers: new HttpHeaders({
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
				'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
				'Content-Type':  'application/json',				
			  })
		};
	}

	getMessage(condition: Object) : Observable<any>{
		console.log("input:", condition);
		var url: string = this.url + 'get-message';
		return this.http.post<any>(url, JSON.stringify(condition), this.getHttpOption());
	}
	
	sendMessage(condition: Object) : Observable<any> {
		console.log("input send message:", condition);
		var url: string = this.url + 'send-message';
		return this.http.post<any>(url, JSON.stringify(condition), this.getHttpOption());
	}
	
}