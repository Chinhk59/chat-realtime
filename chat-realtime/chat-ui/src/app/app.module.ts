 import { BrowserModule } from '@angular/platform-browser';
 import { NgModule } from '@angular/core';
 
 import { AppComponent } from './app.component';
 import { AppRoutingModule } from './app-routing.module';
 import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 import { Router } from '@angular/router';
 
 import { HttpClient, HttpClientModule } from '@angular/common/http';
 import { FormsModule } from '@angular/forms';
 import { ChatService } from './chat.service';
 import { TooltipModule } from 'ng2-tooltip-directive';

 @NgModule({
   declarations: [
     AppComponent
   ],
   imports: [
     BrowserModule,
     BrowserAnimationsModule,
     AppRoutingModule,
     FormsModule,
     HttpClientModule,
     TooltipModule,
   ],
   providers: [
     HttpClient,
     ChatService,
   ],
   bootstrap: [AppComponent]
 })
 export class AppModule {
   constructor(router: Router) {}
 }
 