package vn.com.lendbiz.kafkachat.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ClientInfo {
	private String clientName;
	private String clientType;
	private String clientRole;
	private String userId;
}
