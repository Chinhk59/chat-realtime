package vn.com.lendbiz.kafkachat.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Message {
	private String id;
	private String key;
	private String message;
	private String userName;
	private Timestamp crtDate;
	private boolean isNewTopic;
	
	@Override
    public String toString() {
        return "Message{" +
        		"id='" + id + '\'' +
        		"key='" + key + '\'' +
                "sender='" + userName + '\'' +
                ", content='" + message + '\'' +
                ", timestamp='" + crtDate + '\'' +
                '}';
    }
}
