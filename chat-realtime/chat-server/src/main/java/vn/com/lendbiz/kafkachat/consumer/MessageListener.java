package vn.com.lendbiz.kafkachat.consumer;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import vn.com.lendbiz.kafkachat.constants.Constants;
import vn.com.lendbiz.kafkachat.entity.ChatContent;
import vn.com.lendbiz.kafkachat.model.Message;
import vn.com.lendbiz.kafkachat.model.base.BaseObject;
import vn.com.lendbiz.kafkachat.repository.ChatContentRepository;

@Component
public class MessageListener extends BaseObject {
    @Autowired
    SimpMessagingTemplate template;
    @Autowired
    ChatContentRepository chatContentRepository;

    @KafkaListener(
            topics = Constants.KAFKA_TOPIC,
            groupId = Constants.GROUP_ID
    )
    public void listen(Message message) {
        logger.info("get message from queue", message);
        /** Save message to DB **/
        ChatContent content = new ChatContent();
        content.setId(UUID.randomUUID().toString());
        content.setKey(message.getKey());
        content.setMessage(message.getMessage());
        content.setUserName(message.getUserName());
        content.setCrtDate(message.getCrtDate());
        
        try {
        	ChatContent obj = chatContentRepository.save(content);
        	message.setId(obj.getId());
		} catch (Exception e) {
			logger.info("Error when save content to DB");
		}
        	
        if (message.isNewTopic()) {
        	template.convertAndSend("/topic/group/create-topic", message);
        } else {
        	template.convertAndSend("/topic/group/send-msg", message);
        }
        
    }
}
