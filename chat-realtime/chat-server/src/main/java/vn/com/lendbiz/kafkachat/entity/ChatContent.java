package vn.com.lendbiz.kafkachat.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "CHAT_CONTENT")
@NamedQuery(name = "ChatContent.findAll", query = "SELECT c FROM ChatContent c")
@Setter
@Getter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatContent {

	@Id
	private String id;
	
	@Column(name = "KEY")
	private String key;
	
	@Column(name = "MESSAGE")
	private String message;
	
	@Column(name = "USER_NAME")
	private String userName;
	
	@Column(name = "CREATE_DATE")
	private Timestamp crtDate;
}
