package vn.com.lendbiz.kafkachat.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "CHAT_INFO")
@NamedQuery(name = "ChatInfo.findAll", query = "SELECT c FROM ChatInfo c")
@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatInfo {
	
	@Id
	private String id;
	
	@Column(name = "key")
	private String key;

	@Column(name = "CUSTID")
	private String custId;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "ROLE")
	private String role;
	
	@Column(name = "USER_NAME")
	private String userName;
	
	@Column(name = "CREATE_DATE")
	private Timestamp crtDate;

}
