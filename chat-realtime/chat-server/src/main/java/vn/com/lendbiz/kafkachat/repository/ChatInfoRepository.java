package vn.com.lendbiz.kafkachat.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.com.lendbiz.kafkachat.entity.ChatInfo;

@Repository
public interface ChatInfoRepository extends JpaRepository<ChatInfo, String> {

	Optional<ChatInfo> findByKey(String key);
	
	List<ChatInfo> findByTypeAndRole(String type, String role);
	
	Optional<ChatInfo> findByKeyAndRole(String key, String role);
	
	
}
