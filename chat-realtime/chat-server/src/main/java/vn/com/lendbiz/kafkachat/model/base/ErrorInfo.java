package vn.com.lendbiz.kafkachat.model.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorInfo extends BaseObject {
	private int status;
	private String error;
	private String errorCode;
	private String errorDesc;
	private String path;

	//Optional
//	private Object data;
}
