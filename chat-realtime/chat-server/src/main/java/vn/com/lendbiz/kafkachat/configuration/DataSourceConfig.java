package vn.com.lendbiz.kafkachat.configuration;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

@Configuration
public class DataSourceConfig {

	@Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${oracle.datasource.minPoolSize}")
    private int minPoolSize;

    @Value("${oracle.datasource.maxPoolSize}")
    private int maxPoolSize;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    @Configuration
    @Profile("oracle-ucp")
    public class OracleUCPConfiguration {
     
        @Bean
        public DataSource dataSource() throws SQLException {
            PoolDataSource dataSource = PoolDataSourceFactory.getPoolDataSource();
            dataSource.setUser(username);
            dataSource.setPassword(password);
            dataSource.setConnectionFactoryClassName(driverClassName);
            dataSource.setURL(url);
            dataSource.setFastConnectionFailoverEnabled(true);
            dataSource.setInitialPoolSize(5);
            dataSource.setMinPoolSize(minPoolSize);
            dataSource.setMaxPoolSize(maxPoolSize);
            return dataSource;
        }
    }
}
