package vn.com.lendbiz.kafkachat.service.impl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import vn.com.lendbiz.kafkachat.constants.Constants;
import vn.com.lendbiz.kafkachat.dto.request.ChatRequest;
import vn.com.lendbiz.kafkachat.dto.request.GetMessageInput;
import vn.com.lendbiz.kafkachat.dto.response.ChatResponse;
import vn.com.lendbiz.kafkachat.dto.response.MessageResult;
import vn.com.lendbiz.kafkachat.entity.ChatContent;
import vn.com.lendbiz.kafkachat.entity.ChatInfo;
import vn.com.lendbiz.kafkachat.model.ClientInfo;
import vn.com.lendbiz.kafkachat.model.ErrorInfo;
import vn.com.lendbiz.kafkachat.model.Message;
import vn.com.lendbiz.kafkachat.model.ServerInfo;
import vn.com.lendbiz.kafkachat.model.exception.BusinessException;
import vn.com.lendbiz.kafkachat.model.exception.InputInvalidExeption;
import vn.com.lendbiz.kafkachat.repository.ChatContentRepository;
import vn.com.lendbiz.kafkachat.repository.ChatInfoRepository;
import vn.com.lendbiz.kafkachat.service.ChatService;
import vn.com.lendbiz.kafkachat.utils.ErrorCode;
import vn.com.lendbiz.kafkachat.utils.StringUtil;

@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    private KafkaTemplate<String, Message> kafkaTemplate;
    
    @Autowired
    private ChatInfoRepository chatInfoRepository;
    
    @Autowired
    private ChatContentRepository chatContentRepo;
    
	@Override
	@Transactional
	public MessageResult sendMessage(ChatRequest request) throws InputInvalidExeption {
		ErrorInfo errorInfo = validateInput(request);
		if (!errorInfo.getErrorCode().equalsIgnoreCase(ErrorCode.VALID))
			throw new InputInvalidExeption(errorInfo.getErrorCode(), errorInfo.getMessage());
		
		/** Send message**/
		Message message = new Message();
		message.setKey(request.getKey());
		message.setUserName(request.getUserName());
		message.setMessage(request.getMessage());
		message.setCrtDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		
		/** Check key is exist**/
		Optional<ChatInfo> chatInfo = chatInfoRepository.findByKeyAndRole(request.getKey(), request.getRole());
		if (!chatInfo.isPresent()) {
			/**Create new key chat**/
			ChatInfo info = new ChatInfo();
			info.setId(UUID.randomUUID().toString());
			info.setKey(request.getKey());
			info.setCustId(request.getUserId());
			info.setRole(request.getRole());
			info.setType(request.getType());
			info.setUserName(request.getUserName());
			info.setCrtDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			chatInfoRepository.save(info);
			
			message.setNewTopic(true);
			try {
				kafkaTemplate.send(Constants.KAFKA_TOPIC, message).get();
			} catch (InterruptedException | ExecutionException e) {
	            throw new RuntimeException(e);
	        }
			
			return new MessageResult("Send message success !");
		}

		message.setNewTopic(false);
		try {
            //Sending the message to kafka topic queue
            kafkaTemplate.send(Constants.KAFKA_TOPIC, message).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        
		return new MessageResult("Send message success !");
	}

	private ErrorInfo validateInput(ChatRequest request) {
		if (StringUtil.isEmty(request.getKey()))
			return new ErrorInfo(ErrorCode.KEY_INVALID, ErrorCode.getMessage(ErrorCode.KEY_INVALID));
		if (StringUtil.isEmty(request.getMessage()))
			return new ErrorInfo(ErrorCode.MESSAGE_INVALID, ErrorCode.getMessage(ErrorCode.MESSAGE_INVALID));
		if (StringUtil.isEmty(request.getRole()))
			return new ErrorInfo(ErrorCode.ROLE_INVALID, ErrorCode.getMessage(ErrorCode.ROLE_INVALID));
		if (StringUtil.isEmty(request.getType()))
			return new ErrorInfo(ErrorCode.TYPE_INVALID, ErrorCode.getMessage(ErrorCode.TYPE_INVALID));
		if (StringUtil.isEmty(request.getUserId()))
			return new ErrorInfo(ErrorCode.USERID_INVALID, ErrorCode.getMessage(ErrorCode.USERID_INVALID));
		if (StringUtil.isEmty(request.getUserName()))
			return new ErrorInfo(ErrorCode.USERNAME_INVALID, ErrorCode.getMessage(ErrorCode.USERNAME_INVALID));
		
		return new ErrorInfo(ErrorCode.VALID, ErrorCode.getMessage(ErrorCode.VALID));
	}

	@Override
	public List<ChatResponse> getMessage(GetMessageInput request) throws BusinessException {
		if (!StringUtil.isEmty(request.getKey())) 
			return getMessageByKey(request.getKey(), request.getPage());
		
		if (!StringUtil.isEmty(request.getRole()) && !StringUtil.isEmty(request.getType())) 
			return getMessageByType(request.getRole(), request.getType(), request.getPage());
		
		return new LinkedList<ChatResponse>();
	}

	private List<ChatResponse> getMessageByType(String role, String type, int pageNumber) throws BusinessException {
		/** Check role **/
		if (!role.equalsIgnoreCase(Constants.ROLE_ADMIN))
			throw new BusinessException(ErrorCode.PERMISSION_ERROR, ErrorCode.getMessage(ErrorCode.PERMISSION_ERROR));
		/** Get list key by type**/
		List<ChatInfo> infos = chatInfoRepository.findByTypeAndRole(type, Constants.ROLE_GUEST);
		LinkedList<ChatResponse> result = new LinkedList<ChatResponse>();
		
		if (CollectionUtils.isEmpty(infos))
			return result;
		
		for (ChatInfo chatInfo : infos) {
//			Page<ChatContent> page = chatContentRepo.findAll(PageRequest.of(pageNumber, 200));
//			List<ChatContent> chatContents = page.getContent();
			result.addAll(getMessageByKey(chatInfo.getKey(), pageNumber));
		}
		
		/** Sort data by crtTime Desc**/
		Collections.sort(result, (c1, c2) -> {
			return c2.getListContent().get(c2.getListContent().size()-1).getCrtDate().compareTo(c1.getListContent().get(c1.getListContent().size()-1).getCrtDate());
		});
		
		return result;
		
	}

	private List<ChatResponse> getMessageByKey(String key, int pageNumber) {
		
		List<ChatContent> contents = chatContentRepo.findByKeyOrderDate(key);
		LinkedList<ChatResponse> result = new LinkedList<ChatResponse>();
		if (CollectionUtils.isEmpty(contents))
			return result;
		
		ChatResponse chatResult = new ChatResponse();
		chatResult.setKey(key);
		chatResult.setListContent(contents);
		
		/** Get info client **/
		Optional<ChatInfo> clientInfo = chatInfoRepository.findByKeyAndRole(key, Constants.ROLE_GUEST);
		if(clientInfo.isPresent()) {
			ChatInfo info = clientInfo.get();
			ClientInfo client = new ClientInfo();
			client.setClientName(info.getUserName());
			client.setClientRole(info.getRole());
			client.setClientType(info.getType());
			client.setUserId(info.getCustId());
			
			chatResult.setClientInfo(client);
		}
		
		/** Get info server **/
		Optional<ChatInfo> serverInfo = chatInfoRepository.findByKeyAndRole(key, Constants.ROLE_ADMIN);
		if(serverInfo.isPresent()) {
			ChatInfo info = serverInfo.get();
			ServerInfo server = new ServerInfo();
			server.setServerName(info.getUserName());
			server.setServerRole(info.getRole());
			server.setServerType(info.getType());
			server.setUserId(info.getCustId());
			
			chatResult.setServerInfo(server);
		}
		
		result.add(chatResult);
		return result;
	}

}
