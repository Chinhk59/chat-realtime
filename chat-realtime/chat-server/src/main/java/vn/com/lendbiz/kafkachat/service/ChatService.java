package vn.com.lendbiz.kafkachat.service;

import java.util.List;

import vn.com.lendbiz.kafkachat.dto.request.ChatRequest;
import vn.com.lendbiz.kafkachat.dto.request.GetMessageInput;
import vn.com.lendbiz.kafkachat.dto.response.ChatResponse;
import vn.com.lendbiz.kafkachat.dto.response.MessageResult;
import vn.com.lendbiz.kafkachat.model.exception.BusinessException;
import vn.com.lendbiz.kafkachat.model.exception.InputInvalidExeption;

public interface ChatService {
	
	public MessageResult sendMessage(ChatRequest request) throws InputInvalidExeption;

	public List<ChatResponse> getMessage(GetMessageInput request) throws BusinessException;
}
