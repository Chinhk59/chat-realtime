package vn.com.lendbiz.kafkachat.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GetMessageInput {

	private String key;
	private String userId;
	private String role;
	private String type;
	private int page;
}
