package vn.com.lendbiz.kafkachat.utils;

import java.util.HashMap;
import java.util.Map;

public class ErrorCode {

	public static final String VALID = "01";
	public static final String PERMISSION_ERROR = "08";
	public static final String KEY_INVALID = "021";
	public static final String MESSAGE_INVALID = "022";
	public static final String ROLE_INVALID = "023";
	public static final String TYPE_INVALID = "024";
	public static final String USERID_INVALID = "025";
	public static final String USERNAME_INVALID = "026";
	
	public static String getMessage(String code) {
		Map<String, String> errorInfo = new HashMap<String, String>();
		errorInfo.put(VALID, "data is valid");
		errorInfo.put(PERMISSION_ERROR, "User does not have permission");
		errorInfo.put(KEY_INVALID, "Key invalid");
		errorInfo.put(MESSAGE_INVALID, "Message invalid");
		errorInfo.put(ROLE_INVALID, "Role invalid");
		errorInfo.put(TYPE_INVALID, "Type invalid");
		errorInfo.put(USERID_INVALID, "UserId invalid");
		errorInfo.put(USERNAME_INVALID, "UserName invalid");
		return errorInfo.get(code);
	}
}
