package vn.com.lendbiz.kafkachat.model.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class InputInvalidExeption extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorCode;
	private String errorDesc;
}
