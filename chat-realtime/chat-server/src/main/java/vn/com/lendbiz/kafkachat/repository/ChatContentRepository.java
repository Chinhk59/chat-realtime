package vn.com.lendbiz.kafkachat.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import vn.com.lendbiz.kafkachat.entity.ChatContent;

@Repository
public interface ChatContentRepository extends JpaRepository<ChatContent, Long> {

	@Query(value =  "SELECT * FROM chat_content WHERE key = ?1 ORDER BY create_date ASC", nativeQuery = true)
	List<ChatContent> findByKeyOrderDate(String key);
	
	@Query(value = "SELECT * FROM chat_content ORDER BY CREATE_DATE ASC", nativeQuery = true)
	List<ChatContent> findAllOrderDate();
}
