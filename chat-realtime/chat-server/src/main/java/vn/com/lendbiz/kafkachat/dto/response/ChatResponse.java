package vn.com.lendbiz.kafkachat.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.lendbiz.kafkachat.entity.ChatContent;
import vn.com.lendbiz.kafkachat.model.ClientInfo;
import vn.com.lendbiz.kafkachat.model.ServerInfo;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ChatResponse {

	private List<ChatContent> listContent;
	
	private String key;
	
	private ClientInfo clientInfo;
	
	private ServerInfo serverInfo;
}
