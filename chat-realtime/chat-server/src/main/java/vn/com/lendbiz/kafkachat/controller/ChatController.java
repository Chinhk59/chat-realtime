package vn.com.lendbiz.kafkachat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import vn.com.lendbiz.kafkachat.dto.request.ChatRequest;
import vn.com.lendbiz.kafkachat.dto.request.GetMessageInput;
import vn.com.lendbiz.kafkachat.dto.response.ChatResponse;
import vn.com.lendbiz.kafkachat.dto.response.MessageResult;
import vn.com.lendbiz.kafkachat.model.Message;
import vn.com.lendbiz.kafkachat.model.base.BaseObject;
import vn.com.lendbiz.kafkachat.model.exception.BusinessException;
import vn.com.lendbiz.kafkachat.model.exception.InputInvalidExeption;
import vn.com.lendbiz.kafkachat.service.ChatService;

@RestController
@RequestMapping("/chat/v1.0")
@RequiredArgsConstructor
public class ChatController extends BaseObject {

    @Autowired
    private ChatService chatService;

    @PostMapping(value = "/get-message")
    public List<ChatResponse> getMessage(@RequestBody GetMessageInput request) throws BusinessException {
    	return chatService.getMessage(request);
    }
    
    @PostMapping(value = "/send-message", consumes = "application/json", produces = "application/json")
    public MessageResult sendMessage(@RequestBody ChatRequest request) throws InputInvalidExeption {
    	return chatService.sendMessage(request);
    }

    //    -------------- WebSocket API ----------------
    @MessageMapping("/sendMessage/{key}")
    @SendTo("/topic/group/{key}")
    public Message broadcastGroupMessage(@DestinationVariable String key, @Payload Message message) {
        return message;
    }
    
    @MessageMapping("/newUser")
    @SendTo("/topic/group")
    public Message addUser(@Payload Message message,
                           SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", message.getUserName());
        return message;
    }

}
