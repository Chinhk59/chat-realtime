package vn.com.lendbiz.kafkachat.constants;

public class Constants {
    public static final String KAFKA_TOPIC = "chat-realtime";
    public static final String GROUP_ID = "kafka-sandbox";
    public static final String KAFKA_BROKER = "localhost:9092";
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_GUEST = "GUEST";
}
