package vn.com.lendbiz.kafkachat.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ChatRequest {

	private String key;
	private String message;
	private String userName;
	private String type;
	private String role;
	private String userId;
}
