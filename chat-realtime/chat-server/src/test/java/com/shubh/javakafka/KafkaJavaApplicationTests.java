package com.shubh.javakafka;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import vn.com.lendbiz.kafkachat.KafkaJavaApp;

@SpringBootTest(classes = KafkaJavaApp.class)
class KafkaJavaApplicationTests {

    @Test
    void contextLoads() {
    }

}
